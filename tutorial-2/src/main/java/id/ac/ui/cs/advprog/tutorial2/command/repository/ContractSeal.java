package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    Spell latestSpell;

    public ContractSeal() {
        spells = new HashMap<>();
        latestSpell = null;
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        Spell theSpell = spells.get(spellName);
        theSpell.cast();
        latestSpell = theSpell;

        // TODO: Complete Me
    }

    public void undoSpell() {
        if(latestSpell != null) {
            latestSpell.undo();
            latestSpell = null;
        }

            // TODO: Complete Me
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
