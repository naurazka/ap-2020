package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;
public class FamiliarSummonSpell extends FamiliarSpell {
	// TODO: Complete Me

    @Override
    public String spellName() {
        return familiar.getRace() + ":Summon";
    }

    public void cast() {
        familiar.summon();
    }

    public FamiliarSummonSpell(Familiar familiar) {
        super(familiar);
    }


}
