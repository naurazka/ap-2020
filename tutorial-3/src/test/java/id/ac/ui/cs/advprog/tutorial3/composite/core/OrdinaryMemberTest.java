package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        // TODO: Complete me
        assertEquals("Nina", member.getName());
    }


    @Test
    public void testMethodGetRole() {
        // TODO: Complete me
        assertEquals("Merchant", member.getRole());
    }


    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        // TODO: Complete me
        int bfr = member.getChildMembers().size();
        Member coba = new OrdinaryMember("test", "testRole");
        member.addChildMember(coba);
        assertEquals(member.getChildMembers().size(), bfr);
        member.removeChildMember(coba);
        assertEquals(member.getChildMembers().size(), bfr);
    }

}
