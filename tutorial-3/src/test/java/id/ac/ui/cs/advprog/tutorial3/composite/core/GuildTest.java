package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        // TODO: Complete me
        int bfr = guildMaster.getChildMembers().size();
        Member coba = new PremiumMember("test", "testRole");
        guild.addMember(guildMaster, coba);
        assertEquals(guildMaster.getChildMembers().size(), bfr + 1);
    }


    @Test
    public void testMethodRemoveMember() {
        Member coba = new PremiumMember("test", "testRole");
        guild.addMember(guildMaster, coba);
        int bfr = guildMaster.getChildMembers().size();
        guild.removeMember(guildMaster, coba);
        assertEquals(guildMaster.getChildMembers().size(), bfr - 1);
        //TODO: Complete me
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Sasuke", "Uchiha");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        // TODO: Complete me
        Member coba = new PremiumMember("test", "testRole");
        guild.addMember(guildMaster, coba);
        assertEquals(coba, guild.getMember("test", "testRole"));
    }

}
