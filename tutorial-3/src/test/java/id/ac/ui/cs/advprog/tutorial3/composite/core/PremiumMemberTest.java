package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Gold Merchant", member.getRole());
        //TODO: Complete me

    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        int bfr = member.getChildMembers().size();
        Member coba = new PremiumMember("test", "testRole");
        member.addChildMember(coba);
        assertEquals(member.getChildMembers().size(), bfr + 1);

    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member coba = new PremiumMember("test", "testRole");
        member.addChildMember(coba);
        int bfr = member.getChildMembers().size();
        member.removeChildMember(coba);
        assertEquals(member.getChildMembers().size(), bfr - 1);

    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me

        Member test1 = new PremiumMember("test1", "testRole1");
        Member test2 = new PremiumMember("test2", "testRole2");
        member.addChildMember(test1);
        member.addChildMember(test2);
        int bfr = member.getChildMembers().size();
        Member test3 = new PremiumMember("test3", "testRole3");
        Member test4 = new PremiumMember("test4", "testRole4");
        member.addChildMember(test3);
        member.addChildMember(test4);
        assertEquals(member.getChildMembers().size(), bfr+1);

    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member guild_master = new PremiumMember("Masterguild", "Master");
        Member test1 = new PremiumMember("test1", "testRole1");
        Member test2 = new PremiumMember("test2", "testRole2");
        Member test3 = new PremiumMember("test3", "testRole3");
        guild_master.addChildMember(test1);
        guild_master.addChildMember(test2);
        guild_master.addChildMember(test3);
        int bfr = guild_master.getChildMembers().size();
        Member test4 = new PremiumMember("test4", "testRole4");
        guild_master.addChildMember(test4);
        assertEquals(guild_master.getChildMembers().size(), bfr + 1);

    }
}
