package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {

    private String name;
    private String role;
    private List<Member> childMembers;

    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
        this.childMembers = new ArrayList<Member>();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public void addChildMember(Member member) {
        if (this.getRole().equals("Master")) {
            this.childMembers.add(member);
        } else {
            if (this.childMembers.size() < 3) {
                this.childMembers.add(member);
            }
        }
    }

    @Override
    public void removeChildMember(Member member) {
        this.childMembers.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return this.childMembers;
    }
}
