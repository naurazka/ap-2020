package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyService academyService;

    // TODO create tests

    public void whenProduceKnightTrigerredShouldCallGetAcademyByName() {
        academyService.produceKnight("Lordran", "majestic");

        verify(academyRepository, times(1)).getKnightAcademyByName("Lordran");
    }

    public void whenGetKnightAcademiesTrigerredShouldCallGetKnightAcadRepo() {
        academyService.getKnightAcademies();
        verify(academyRepository, times(1)).getKnightAcademies();
    }

}
