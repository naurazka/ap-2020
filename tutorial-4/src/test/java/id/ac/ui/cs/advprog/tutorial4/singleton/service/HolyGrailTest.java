package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {
    private HolyGrail holyGrail;

    public void testGetAWish() {
        assertTrue(holyGrail.getHolyWish() instanceof HolyWish);
    }


    // TODO create tests
}
