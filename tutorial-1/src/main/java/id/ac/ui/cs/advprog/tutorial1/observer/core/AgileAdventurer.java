package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                this.guild = guild;
                //ToDo: Complete Me
        }

        public void update() {
                String questType = guild.getQuestType();
                if (questType.compareTo("D") == 0 || questType.compareTo("R") == 0) {
                        getQuests().add(guild.getQuest());
                }
                ;
        }

        //ToDo: Complete Me
}
