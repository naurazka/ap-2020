package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
                //ToDo: Complete Me
        }

        public void update() {
                String questType = guild.getQuestType();
                if (questType.compareTo("D") == 0 || questType.compareTo("E") == 0) {
                        getQuests().add(guild.getQuest());
                }
                ;
        }

        //ToDo: Complete Me
}
