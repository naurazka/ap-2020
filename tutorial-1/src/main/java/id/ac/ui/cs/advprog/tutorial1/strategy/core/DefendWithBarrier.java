package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    public String defend(){
        return "Aku punya barrier";
    }

    public String getType(){
        return "perlindungan dengan barrier";
    }
        //ToDo: Complete me
}
